from typing import Callable, Set, Dict

from discord import TextChannel


class Updatable:

    def __init__(self) -> None:
        self.channels: Dict[TextChannel, Dict[str, Set[str]]] = dict()

    async def check_updates(self) -> None:
        raise NotImplementedError()
    
    async def update_channel(self, channel: TextChannel, entry) -> None:
        raise NotImplementedError()

    def add_filter(self, channel: TextChannel, field: str, filter_str: str) -> None:
        if field not in self.channels[channel]:
            self.channels[channel][field] = set()

        self.channels[channel][field].add(filter_str)

    def remove_filter(self, channel: TextChannel, field: str, filter_str: str) -> None:
        if field in self.channels[channel]:
            self.channels[channel][field].remove(filter_str)
    
    def __call__(self):
        return self.check_updates()

    def __iadd__(self, channel: TextChannel):
        if channel not in self.channels:
            self.channels[channel] = dict()
        return self

    def __isub__(self, channel: TextChannel):
        del self.channels[channel]
        return self

    def __iter__(self):
        return iter(self.channels.keys())
