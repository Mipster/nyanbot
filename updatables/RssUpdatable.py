from typing import Set

import feedparser

from updatables.Updatable import Updatable


class RssUpdatable(Updatable):

    def __init__(self, urlstring: str) -> None:
        super().__init__()
        self.url = urlstring
        self.last_feed_hash: Set[str] = set()

    def hash_entry(self, entry) -> str:
        raise NotImplementedError()

    async def check_updates(self) -> None:
        feed = feedparser.parse(self.url)
        current_feed: Set[str] = set()
        new_entries = set()

        for entry in feed.entries:
            entry_hash = self.hash_entry(entry)
            current_feed.add(entry_hash)

            if entry_hash not in self.last_feed_hash:
                new_entries.add(entry)

        for entry in new_entries:
            for channel, filters in self.channels.items():
                if any(map(
                    lambda field_vals: field_vals[0] in entry and any(map(
                        lambda val: val in entry[field_vals[0]], field_vals[1]
                    )),
                    filters.items()
                )):
                    await self.update_channel(channel, entry)
                    break

        self.last_feed_hash = current_feed
