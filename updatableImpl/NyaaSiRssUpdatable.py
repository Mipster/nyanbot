import urllib.parse
from discord import TextChannel, Embed

from updatables.RssUpdatable import RssUpdatable


class NyaaSiRssUpdatable(RssUpdatable):

    NYAA_SI_RSS_URL = 'https://nyaa.si/?page=rss'

    def __init__(self) -> None:
        super().__init__(NyaaSiRssUpdatable.NYAA_SI_RSS_URL)

    @staticmethod
    def generate_magent_link(entry):
        params = {
            'xt': f"urn:btih:{entry.nyaa_infohash}",
            'dn': f"{entry.title}",
            'tr': [
                "http://nyaa.tracker.wf:7777/announce",
                "udp://open.stealth.si:80/announce",
                "udp://tracker.opentrackr.org:1337/announce",
                "udp://tracker.coppersurfer.tk:6969/announce",
                "udp://exodus.desync.com:6969/announce"
            ]
        }

        return f"magnet:?{urllib.parse.urlencode(params, doseq=True)}"

    async def update_channel(self, channel: TextChannel, entry):
        embed_msg = Embed()

        embed_msg.title = "Update found!"

        embed_msg.description = (
            f"**Title:** {entry.title}\n"
            f"**nyaa.si link:** {entry.id}\n"
            f"**Torrent link:** {entry.link}\n"
            f"**Magnet link:**\n{NyaaSiRssUpdatable.generate_magent_link(entry)}"
        )

        await channel.send(embed=embed_msg)

    def hash_entry(self, entry) -> str:
        if 'nyaa_infohash' in entry:
            return entry.nyaa_infohash
        
        return super().hash_entry(entry)
