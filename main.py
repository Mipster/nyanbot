import os

from discord.ext import commands
from dotenv import load_dotenv

from NyanCog import NyanCog
from updatableImpl.NyaaSiRssUpdatable import NyaaSiRssUpdatable

AVAILABLE_UPDATABLES = {
    'nyaa.si': NyaaSiRssUpdatable()
}


def main():
    load_dotenv(dotenv_path=os.getenv('DOTENV_LOCATION') or ".env")
    print(f"RUNNING ENV: {os.getenv('env')}")

    token = os.getenv('DISCORD_TOKEN')

    bot = commands.Bot(command_prefix=';')

    bot.add_cog(NyanCog(
        bot,
        AVAILABLE_UPDATABLES,
        int(os.getenv('interval_seconds'))
    ))

    bot.run(token)


if __name__ == '__main__':
    main()
