from typing import Optional, Dict

from discord.ext import commands, tasks
from discord.ext.commands import Bot

from updatables.Updatable import Updatable


class NyanCog(commands.Cog):

    def __init__(self, bot: Bot,
                 available_updatables: Optional[Dict[str, Updatable]] = None,
                 interval_seconds: float = 10 * 60):
        self.bot = bot
        self.interval_seconds = interval_seconds
        self.available_updatables: Dict[str, Updatable] = available_updatables or dict()

        bot.add_listener(self.on_ready)

    async def on_ready(self):
        tasks.loop(seconds=self.interval_seconds)(self.check_updates).start()

    @commands.command()
    async def subscribe(self, ctx, updatable: Optional[str] = None, channel_name: Optional[str] = None):
        """Subscribe to streams updates in given channel"""

        if updatable == "help":
            available_updatable_str = '\n\t'.join(self.available_updatables.keys())
            await ctx.send(
                "```"
                "Usage: subscribe <stream> <channel_name>\n"
                "stream: the update stream you wish to recive updates from\n"
                "channel_name: the channel name in the server you want the updates to appear in\n"
                "Available streams:\n"
                f"\t{available_updatable_str}"
                "```"
            )
            return

        if updatable is None or updatable not in self.available_updatables:
            available_updatable_str = '\n\t'.join(self.available_updatables.keys())
            await ctx.send(f'```Stream no available\nAvailable streams:\n\t{available_updatable_str}```')
            return

        channel = next(filter(lambda a: a.name == channel_name, ctx.guild.channels), None)

        if channel is None:
            await ctx.send(f'```Channel not available\nplease select a channel name that exist in the server```')
        else:
            if channel not in self.available_updatables[updatable]:
                self.available_updatables[updatable] += channel
                await ctx.send(f'```Subscribed to stream "{updatable}" in channel "{channel_name}"```')
            else:
                await ctx.send(f'```Channel "{channel_name}" is already subscribed to "{updatable}"!```')

    @commands.command()
    async def filter(self, ctx, updatable: Optional[str] = None, channel_name: Optional[str] = None,
                     field: Optional[str] = None, filter_str: Optional[str] = None):
        """Adds filters per stream per channel by checking string inclution in an entry field"""

        if updatable == 'help':
            await ctx.send(
                "```"
                "Usage: filter <stream> <channel_name> <field> <filter>\n"  # [is_whitelist]"
                "    channel_name:   the name of the channel you want the filter to work on\n"
                "    field:          the data field you want to run the filter on\n"
                "    filter:         the string that need to be in the field value "
                "(or not if is_whitelist is false) to be sent to the channel"
                # "Options:"
                # "    is_whitelist:   default value is true, if false the filter will not display matching patterns"
                "```"
            )
            return

        if updatable is None or updatable not in self.available_updatables:
            available_updatable_str = '\n\t'.join(self.available_updatables.keys())
            await ctx.send(f'```Stream not available\nAvailable streams:\n\t{available_updatable_str}```')
            return

        channel = next(filter(lambda a: a.name == channel_name, ctx.guild.channels), None)

        if channel is None:
            await ctx.send(f'```Channel not available\nplease select a channel name that exist in the server```')
        elif field is None or filter_str is None:
            await ctx.send(f'```Field and Filter must be given```')

        self.available_updatables[updatable].add_filter(channel, field, filter_str)

        await ctx.send(
            "```"
            f"Added filter to stream: {updatable} on channel: {channel_name}\n"
            f"that the field: {field} contains {filter_str}"
            "```"
        )

    async def check_updates(self):
        for updatable in self.available_updatables.values():
            await updatable.check_updates()
